#include <Wire.h>
int dataReceived = 0;
int i=0;
byte i2cTrame[32];
byte mode = 3; //always run
#define SLAVE_ADDRESS 0x12
#define GATE 13 //pin du mosfet
unsigned long wakeUpTime=0;
unsigned long sleepTime=0;

void setup() {
  Serial.begin(9600);
  Wire.begin(SLAVE_ADDRESS);
  Wire.onReceive(receiveData);
  digitalWrite(GATE,HIGH);
}

void loop() {

  unsigned long currentMillis = millis();
  if(mode==1){ //go to sleep
     //avr, ect...
  }
  else if(mode==2){ //sleeping
    if ((unsigned long)(currentMillis - sleepTime) >= wakeUpTime){ //normalement c'est roll-over safe
     digitalWrite(GATE,HIGH); 
     Serial.print("Debout !");
     mode=3;
    }
    
  }else{
    
   digitalWrite(GATE,HIGH); 
  }
  
  delay(100);

}

void receiveData(int byteCount){
    while(Wire.available()) {
        dataReceived = Wire.read();
        Serial.print("Donnee recue : ");
        Serial.println(dataReceived);
        i2cTrame[i]=dataReceived;
        i++;
    }
    i=0;
    if(i2cTrame[0]==0x01) //Odre de couper l'alim pendant X secondes
    {
      unsigned long timer= 0;
      timer += (unsigned long)i2cTrame[1] << 24;
      timer += (unsigned long)i2cTrame[2] << 16;
      timer += (unsigned long)i2cTrame[3] << 8;
      timer += (unsigned long)i2cTrame[4];
      Serial.println("SleepFor :");
      Serial.println(timer);
      
      unsigned int delay=0;
      delay += (unsigned int)i2cTrame[5] << 8;
      delay += (unsigned int)i2cTrame[6];

      Serial.println("WithDelay :");
      Serial.println(delay);
      
      mode=1; //go to sleep
      wakeUpTime=timer*1000;
      sleepTime= millis();
    }else if (i2cTrame[0]==0x02){ //always run
      mode=3;
    }
}

/*
mode 1 : go to sleep
mode 2 : sleeping
mode 3 : always run
*/
  
  

