Avertissement
=
Le projet Arduino au sein de PiBatRecorderProjects était une tentative d'extension pour gérer un mode Veille avec une carte Arduino. Du fait de l'arrêt de PiBatRecorder, ce projet n'a pas été utilisé.

L'aventure continue au sein des Teensy Recorder 